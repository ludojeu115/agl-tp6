package couvertureEtMutation;

import java.util.OptionalInt;

public class SUT {
	private int[] tab;
	private int taille=0;

	public SUT(int tailleMax) {
		tab=new int[tailleMax];
	}

	public void ajout(int e) throws TableauPleinException{
		if (taille==tab.length) {
			throw new TableauPleinException();
		}
		tab[taille]=e;
		taille++;
	}

	public int[] values() {
		int[] result=new int[taille];
		for (int i=0;i<taille;i++) {
				result[i]=tab[i];
		}
		
		return result;
	}

	public int retourneEtSupprimePremiereOccurenceMin() throws TableauVideException{
		if (taille==0) {
			throw new TableauVideException();
		}
		int min =tab[0];
		int imin=0;
		for (int i=1;i<taille;i++) {
			if (tab[i]<min) {
				// on a un nouveau min
				min=tab[i];
				imin=i;
			}
		}
		int last = tab[taille-1];
		int current = 0;
		for (int i=taille-1;i>imin;i--) { // parcours du tableau depuis la fin pour décalage à gauche
			current=tab[i-1]; 
			tab[i-1]=last; // suppression et décalage à gauche
			last=current;
		}
		tab[taille-1]=0;
		taille--;
		return min;
	}
}
